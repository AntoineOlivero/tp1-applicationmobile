package com.example.animal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ImageView;

import java.util.Objects;


public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        String saveIntent = getIntent().getExtras().getString("animal");

        final Animal animal = AnimalList.getAnimal(saveIntent);


        final TextView textViewNomAnimal;
        textViewNomAnimal = findViewById(R.id.nomAnimal);
        textViewNomAnimal.setText(saveIntent);

        final ImageView imageViewAnimal = findViewById(R.id.imageAnimal);
        Resources res = getResources();
        String nomImage = animal.getImgFile();
        int IDimg = res.getIdentifier(nomImage, "drawable", getPackageName());
        Drawable drawable = res.getDrawable(IDimg);
        imageViewAnimal.setImageDrawable(drawable);


        final TextView textViewTempsVie = findViewById(R.id.tempsVieValue);
        textViewTempsVie.setText(animal.getStrHightestLifespan());

        final TextView textViewGestation = findViewById(R.id.tempsGestationValue);
        textViewGestation.setText(animal.getStrGestationPeriod());

        final TextView textViewPoidNaissence = findViewById(R.id.poidNassanceValue);
        textViewPoidNaissence.setText(animal.getStrBirthWeight());

        final TextView textViewPoidAdulte = findViewById(R.id.poidAdultValue);
        textViewPoidAdulte.setText(animal.getStrAdultWeight());

        final EditText editText = findViewById(R.id.editTextConcervation);
        editText.setText(animal.getConservationStatus());
        final Button button = findViewById(R.id.buttonEditText);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newStatus = editText.getText().toString();
                animal.setConservationStatus(newStatus);
            }
        });
    }
}
