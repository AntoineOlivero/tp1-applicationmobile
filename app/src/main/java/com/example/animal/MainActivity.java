package com.example.animal;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity {

    final String[] animalName = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final AnimalList animalList = new AnimalList();
        final String[] animalName = AnimalList.getNameArray();

        //final String[] animalName = {"Renard roux", "koala", "Monbéliarde", "Panda géant", "Ours brun", "Chameau", "Lion"};

        final ListView listView = findViewById(R.id.listViewAnimal);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, AnimalList.getNameArray());

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);
                intent.putExtra("animal", animalName[position]);
                startActivity(intent);
            }
        });
    }
}
